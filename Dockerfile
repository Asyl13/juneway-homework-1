FROM debian:9 AS nginx_make

RUN apt update && apt install -y wget gcc make libpcre3-dev
RUN wget http://nginx.org/download/nginx-1.0.5.tar.gz && tar xvfz nginx-1.0.5.tar.gz && cd nginx-1.0.5 && wget https://github.com/PhilipHazel/pcre2/archive/refs/tags/pcre2-10.39.tar.gz && tar xvfz pcre2-10.39.tar.gz && wget http://zlib.net/zlib-1.2.11.tar.gz && tar xvfz zlib-1.2.11.tar.gz && ls -la && pwd && $PWD/configure --with-pcre --with-zlib=$PWD/zlib-1.2.11 && make && make install


FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=nginx_make /usr/local/nginx/ /usr/local/nginx/
RUN ls -la ../ && pwd && touch ../logs/error.log && chmod +x nginx && touch /usr/local/nginx/conf/nginx.conf
RUN ./nginx -v
CMD ["./nginx", "-g", "daemon off;"]
